# Bataille Navale_JS

Bataille navale développée en JS.

## Brief projet

Créer une bataille navale en JS:
- dans un premier temps en simulant le joueur 2 
- puis en permettant à 2 joueurs de jouer en ligne (Node.js + Socket.io) avec persistance des données côté client.
